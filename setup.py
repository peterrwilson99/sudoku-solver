from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()
with open("requirements.txt") as f:
    requirements = f.read().splitlines()


setup(
    name="sudokuSolver",
    version="1.0.0",
    description="Solve sudokus",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Peter Wilson",
    author_email="peterrwilson99@gmail.com",
    url="https://gitlab.com/peterrwilson99/sudoku-solver",
    packages=["sudokuSolver"],
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License (GPL)",
        "Operating System :: OS Independent",
    ],
    install_requires=requirements,
    extras_require={
    },
)
