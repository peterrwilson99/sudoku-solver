#!/usr/bin/env python3
import sys

def main():
    boxes = []
    for i in range(81):
        entry = input(f'Input Box{i+1}: ')
        if entry == '':
            entry = 'X'
        boxes.append(entry)
    lines = []
    iter = 0
    for i in range(9):
        line = []
        for x in range(9):
            line.append(boxes[iter])
            iter +=1
        lines.append(line)
    puzzle = ''
    for line in lines:
        lineOut = ''
        for char in line:
            lineOut += char + ' '
        puzzle += lineOut[:-1] +'\n'
    puzzle = puzzle[:-1]
    print("Puzzle inputted: ")
    print(puzzle)
    flag = input("Save to inputPuzzle.txt?\nEnter 1 to save, anything else to abort: ")
    if flag=="1":
        readobj = open('inputPuzzle.txt','w+')
        readobj.write(puzzle)
        readobj.close()


if __name__ == "__main__":
    main()
