#!/usr/bin/env python3
import sys

box1 = [(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (2, 2)]
box2 = [(0, 3), (0, 4), (0, 5), (1, 3), (1, 4), (1, 5), (2, 3), (2, 4), (2, 5)]
box3 = [(0, 6), (0, 7), (0, 8), (1, 6), (1, 7), (1, 8), (2, 6), (2, 7), (2, 8)]
box4 = [(3, 0), (3, 1), (3, 2), (4, 0), (4, 1), (4, 2), (5, 0), (5, 1), (5, 2)]
box5 = [(3, 3), (3, 4), (3, 5), (4, 3), (4, 4), (4, 5), (5, 3), (5, 4), (5, 5)]
box6 = [(3, 6), (3, 7), (3, 8), (4, 6), (4, 7), (4, 8), (5, 6), (5, 7), (5, 8)]
box7 = [(6, 0), (6, 1), (6, 2), (7, 0), (7, 1), (7, 2), (8, 0), (8, 1), (8, 2)]
box8 = [(6, 3), (6, 4), (6, 5), (7, 3), (7, 4), (7, 5), (8, 3), (8, 4), (8, 5)]
box9 = [(6, 6), (6, 7), (6, 8), (7, 6), (7, 7), (7, 8), (8, 6), (8, 7), (8, 8)]
boxes = [box1, box2, box3, box4, box5, box6, box7, box8, box9]


#maybe have a function called placeNode() where you pass it a list of touching nodes
# and it returns a True if it can be placed in that spot

class puzNode:
    def __init__(self, val):
        if val != "X":
            self.possible = [int(val)]
            self.curState = True
        else:
            self.possible = [1, 2, 3, 4, 5, 6, 7, 8, 9]
            self.curState = False

    def removeOption(self, val):
        try:
            if self.curState == False:
                self.possible.remove(val)
        except:
            pass
        finally:
            if len(self.possible) == 1:
                self.curState = True

    def setSolved(self, val):
        self.curState = True
        self.possible = [val]

    def nodeInfo(self):
        print("Current State: " + str(self.curState))
        if self.curState == False:
            print("Possible Options: " + str(self.possible))
        else:
            print("Value: " + str(self.possible[0]))


def importText():
    textFile = open("inputPuzzle.txt", "r")

    importedPuzzle = []
    x = 0
    while x < 9:
        importedPuzzle.append(textFile.readline())
        x += 1

    tempArray = []
    x = 0
    while x < 9:
        tempArray.append(importedPuzzle[x].split(" "))
        x += 1

    # Following gets rid of new line characters
    x = 0
    while x < 9:
        tempArray[x][8] = tempArray[x][8].strip("\n")
        x += 1

    importPuzzle = tempArray
    return importPuzzle
def createNodePuzzle(importPuzzle):
    nodePuzzle = []
    y = 0
    while y < 9:
        temp = []
        x = 0
        while x < 9:
            temp.append(puzNode((importPuzzle[y][x])))
            x += 1
        nodePuzzle.append(temp)
        y += 1
    return nodePuzzle
def printPuzzleWithNodes(nodePuzzle):
    s1 = "-------------------------"
    print(s1)
    s2 = "|"

    y = 0
    iterator = 0
    while y < 9:
        if y == 3 or y == 6:
            print(s1)
        s2 = "|"
        x = 0
        while x < 9:
            if x == 3 or x == 6:
                s2 = s2 + " |"
            # s2=s2+' '+ str(nodePuzzle[iterator])
            if nodePuzzle[y][x].curState == True:
                s2 = s2 + " " + str(nodePuzzle[y][x].possible[0])
            else:
                s2 = s2 + " X"
            x += 1
        s2 = s2 + " |"
        print(s2)
        y += 1
    print(s1)
def printPuzzleWithText(importPuzzle):
    s1 = "-------------------------"
    print(s1)
    s2 = "|"

    y = 0
    iterator = 0
    while y < 9:
        if y == 3 or y == 6:
            print(s1)
        s2 = "|"
        x = 0
        while x < 9:
            if x == 3 or x == 6:
                s2 = s2 + " |"
            s2 = s2 + " " + str(importPuzzle[y][x])

            x += 1
        s2 = s2 + " |"
        print(s2)
        y += 1
    print(s1)
def nodesSolved(nodePuzzle):
    nodes = 0
    y = 0
    while y < 9:
        x = 0
        while x < 9:
            if nodePuzzle[y][x].curState == True:
                nodes += 1
            x += 1
        y += 1
    return nodes
def testSolution(nodePuzzle):
    # checking rows
    y = 0
    while y < 9:
        x = 0
        temp = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        while x < 9:
            try:
                temp.remove(nodePuzzle[y][x].possible[0])
            except:
                print("Failed Row " + str(y))
                return False
            x += 1
        y += 1
    # checking columns
    while x < 9:
        y = 0
        temp = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        while y < 9:
            try:
                temp.remove(nodePuzzle[y][x].possible[0])
            except:
                print("Failed Column " + str(x))
                return False
            y += 1
        x += 1

    while x < 9:
        y = 0
        temp = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        while y < 9:
            try:
                temp.remove(nodePuzzle[boxes[x][0][y]][boxes[x][1][y]].possible[0])
            except:
                print("Failed Column " + str(x))
                return False
            y += 1
        x += 1

    return True
def printBoxesPossibles(nodePuzzle, box):
    for yiter, xiter in box:
        s = f"Node ({xiter},{yiter}) Possibles: "
        s += str(nodePuzzle[yiter][xiter].possible)
        print(s)

def discoverBox(nodePuzzle, x, y):
    if y < 3:  # row 1 2 or 3
        if x < 3:
            return box1
        elif x < 6:
            return box2
        else:
            return box3

    elif y < 6:  # row 4 5 or 6
        if x < 3:
            return box4
        elif x < 6:
            return box5
        else:
            return box6
    else:
        if x < 3:
            return box7
        elif x < 6:
            return box8
        else:
            return box9

def retrieveNodes(nodePuzzle, x, y):
    nodes = (
        retrieveBoxNodes(nodePuzzle, x, y)
        + retrieveColumnNodes(nodePuzzle, x, y)
        + retrieveRowNodes(nodePuzzle, x, y)
    )
    return list(dict.fromkeys(nodes))
def retrieveBoxNodes(nodePuzzle, x, y):
    nodes = []
    box = discoverBox(nodePuzzle, x, y)
    for yiter, xiter in box:
        if not ((yiter == y) and (xiter == x)):
            nodes.append(nodePuzzle[yiter][xiter])
    return nodes
def retrieveColumnNodes(nodePuzzle, x, y):
    nodes = []
    for yiter in range(9):
        if not (yiter == y):
            nodes.append(nodePuzzle[yiter][x])
    return nodes
def retrieveRowNodes(nodePuzzle, x, y):
    nodes = []
    for xiter in range(9):
        if not (xiter == x):
            nodes.append(nodePuzzle[y][xiter])
    return nodes


def basicElimination(nodePuzzle, x, y):
    touchingNodes = retrieveNodes(nodePuzzle, x, y)
    alreadySolved = []
    for node in touchingNodes:
        if node.curState:
            alreadySolved.append(node.possible[0])
    alreadySolved = list(
        dict.fromkeys(alreadySolved)
    )  # list of touching nodes solved values with no repititions
    for num in alreadySolved:
        nodePuzzle[y][x].removeOption(num)
    return nodePuzzle
def smartElimination(nodePuzzle, x, y):
    touchingNodesLists = [
        retrieveBoxNodes(nodePuzzle, x, y),
        retrieveColumnNodes(nodePuzzle, x, y),
        retrieveRowNodes(nodePuzzle, x, y),
    ]
    for touchingNodes in touchingNodesLists:
        otherPossibilities = []  # A list containing every touching nodes possible values
        for node in touchingNodes:
            if not node.curState:
                otherPossibilities += node.possible
        otherPossibilities = list(dict.fromkeys(otherPossibilities))
        otherPossibilities.sort()
        for num in nodePuzzle[y][x].possible:
            try:
                s = otherPossibilities.index(num)
            except:
                nodePuzzle[y][x].setSolved(num)
                return nodePuzzle
    return nodePuzzle
def pairElimination(nodePuzzle, x, y):
    touchingNodesLists = [
        retrieveBoxNodes(nodePuzzle, x, y),
        retrieveColumnNodes(nodePuzzle, x, y),
        retrieveRowNodes(nodePuzzle, x, y),
    ]
    for touchingNodes in touchingNodesLists:
        exists = [None]*10
        for num in nodePuzzle[y][x].possible:
            if exists[num] is None:
                exists[num] = 1
            else:
                exists[num] += 1

        for node in touchingNodes:
            if not node.curState:
                for num in node.possible:
                    if exists[num] is None:
                        exists[num] = 1
                    else:
                        exists[num] += 1
        #we know each index (ex. '5' represents #5 in puzzle) appears that amount of times
        doubles = []
        index = 0
        for num in exists:
            if num == 2:
                doubles.append(index)
            index +=1
        #we now remove all doubles that one of the two arent in the current node (x ,y)
        index = 0
        while index < len(doubles):
            if doubles[index] not in nodePuzzle[y][x].possible:
                doubles.remove(doubles[index])
                index = index - 1
            index +=1
        for node in touchingNodes:
            appeared = []
            if not node.curState:
                for num in doubles:
                    if num in node.possible:
                        appeared.append(num)
                if len(appeared) == 2:
                    nodePuzzle[y][x].possible = appeared
                    return nodePuzzle
    return nodePuzzle
def notPossibleElimination(nodePuzzle, x, y):
    touchingNodesLists = [
        retrieveBoxNodes(nodePuzzle, x, y),
        retrieveColumnNodes(nodePuzzle, x, y),
        retrieveRowNodes(nodePuzzle, x, y),
    ]
    for touchingNodes in touchingNodesLists:
        notPossibles = [1,2,3,4,5,6,7,8,9]
        for node in touchingNodes:
            for num in node.possible:
                try:
                    notPossibles.remove(num)
                except:
                    pass
        if len(notPossibles) == 1:
            nodePuzzle[y][x].possible = notPossibles
            print('notPossibleElimination Actually worked lol')
            return nodePuzzle
    return nodePuzzle


def solveSudoku(nodePuzzle):
    iterations = 0
    while nodesSolved(nodePuzzle) < 81:
        print("Running Iteration " + str(iterations) + "...")
        for y in range(9):
            for x in range(9):
                if not nodePuzzle[y][x].curState:
                    nodePuzzle = basicElimination(nodePuzzle, x, y)
                    if not nodePuzzle[y][x].curState:
                        nodePuzzle = smartElimination(nodePuzzle, x, y)
                        if not nodePuzzle[y][x].curState:
                            nodePuzzle = notPossibleElimination(nodePuzzle,x,y)
                            if not nodePuzzle[y][x].curState:
                                nodePuzzle = pairElimination(nodePuzzle, x, y)
        iterations += 1

    # NOTE: Suspected solution already found.
    print(
        "Suspected Solution found at "
        + str(iterations)
        + "\nTesting Solution Now.\n\n*************\n\n"
    )
    TestedSolution = testSolution(nodePuzzle)
    if TestedSolution == True:
        print("Solution was found")
        printPuzzleWithNodes(nodePuzzle)
    else:
        print("An error was made...")
        print("Would you like to see the false solution for debugging?")
        flag = input("Enter 1 to view, anything else otherwise: ")
        if flag == "1":
            printPuzzleWithNodes(nodePuzzle)


def main():
    # Import from inputPuzzle.txt to basic array call it importedPuzzle
    # Make puzzle array making a new node for each box in puzzle
    importPuzzle = importText()
    # NOTE: Completed import array.
    nodePuzzle = createNodePuzzle(importPuzzle)
    # NOTE: Have all nodes into nodePuzzle Array.

    printPuzzleWithNodes(nodePuzzle)
    solveSudoku(nodePuzzle)



    # NOTE: add better ways of reducing, Write inputMaker in same format as output


if __name__ == "__main__":
    main()
