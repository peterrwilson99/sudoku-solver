# Sudoku Solver

Creating a Sudoku Solver in Python3, current iteration works mainly on basic functions of sudoku. Currently works on medium sudoku difficulties.
Next is to add functions to make it work for all sudokus, then using a machine learning algorithm (most likely from opencv down the line) to scan an image of a sudoku puzzle and get import the numbers automatically.
inputMaker.py --> inputPuzzle.txt : Input maker is a basic interface to transfer sudokus to the program. sudokuSolver2.py reads inputPuzzle.txt.
